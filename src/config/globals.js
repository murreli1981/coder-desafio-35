require("dotenv").config();
const SERVER_MODE_FROM_ARGS = process.argv[2];
const PORT_FROM_ARGS = process.argv[3];
const FB_CLIENT_ID_FROM_ARGS = process.argv[4];
const FB_CLIENT_SECRET_FROM_ARGS = process.argv[5];

module.exports = {
  PORT: PORT_FROM_ARGS || process.env.PORT || 8080,
  MONGO_URI: process.env.MONGO_URI || "undefined",
  SECRET_KEY: process.env.SECRET_KEY || "qwertyuiop",
  FB_CLIENT_ID: FB_CLIENT_ID_FROM_ARGS || process.env.FB_CLIENT_ID || "id",
  FB_CLIENT_SECRET:
    FB_CLIENT_SECRET_FROM_ARGS || process.env.FB_CLIENT_SECRET || "secret",
  IS_CLUSTER: SERVER_MODE_FROM_ARGS === "CLUSTER" ? true : false,
  LOG_FILE_PATH: process.env.LOG_FILE_PATH,
  ACCOUNT_TWILIO_SID: process.env.ACCOUNT_TWILIO_SID,
  TWILIO_AUTH_TOKEN: process.env.TWILIO_AUTH_TOKEN,
  ADMIN_PHONE_NUMBER: process.env.ADMIN_PHONE_NUMBER,
  FROM_PHONE_NUMBER: process.env.FROM_PHONE_NUMBER,
  NOTIFICATIONS: {
    ETHEREAL: {
      host: "smtp.ethereal.email",
      port: 587,
      secure: false,
      auth: {
        user: process.env.ETHEREAL_EMAIL,
        pass: process.env.ETHEREAL_PASS,
      },
    },
    GMAIL: {
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: process.env.GMAIL_EMAIL,
        pass: process.env.GMAIL_PASS,
      },
    },
  },
};
