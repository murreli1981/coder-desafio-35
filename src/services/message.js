const moment = require("moment");
const messageModel = require("../dao/models/message");
const { normalizeMessages, print } = require("../utils/normalizer");
const { v4: uuidv4 } = require("uuid");
module.exports = class {
  async addMessage(message) {
    message["date"] = `[${moment().format("DD/MM/YYYY hh:mm:ss")}]`;
    message["id"] = Date.now();

    const messageCreated = await messageModel.create(message);
    return messageCreated;
  }
  async getAllMessages() {
    const messages = await messageModel.find().lean();
    const messageCenter = this.messageCenterBuilder(messages);
    const normalizedMsgs = normalizeMessages(messageCenter);
    return normalizedMsgs;
  }

  messageCenterBuilder = (messages) => {
    return { id: 1, content: messages };
  };
};
