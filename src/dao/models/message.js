const { Schema, model } = require("mongoose");

const messageSchema = new Schema({
  id: String,
  author: {
    id: String,
    nombre: String,
    apellido: String,
    edad: Number,
    alias: String,
    avatar: String,
  },
  date: String,
  message: String,
});

module.exports = model("Message", messageSchema);
