# coder-desafio-35

Nodemailer and Twilio

Click en el logo de coder
[![Watch the video](https://res.cloudinary.com/hdsqazxtw/image/upload/v1570710978/coderhouse.jpg)](https://youtu.be/b1pUJzbSWl8)

## endpoints:

[GET]/api/productos - Lista todos los productos

[GET]/api/productos/:id - Trae un único producto por id

[POST] /api/productos - Guarda un producto en la base

[PUT] /api/productos/:id - Modifica un producto por id

[DELETE] /api/productos/:id - Elimina un producto por id

[GET] /info - Muestra información del global process

[GET] /random?cant=99999 - calculo un numero random de 1 a 1000 por la cant de veces recibida

[POST] /forceShutdown - apago el server y muestro por consola el exit code

## UI

https://coder-desafio-33.herokuapp.com/ingreso (autenticado unicamente)

https://coder-desafio-33.herokuapp.com/login

https://coder-desafio-33.herokuapp.com/register

## Inicio del server

nodemon
